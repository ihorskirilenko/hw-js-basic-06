'use strict'

/**
Теорія:

1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

 Екранування - це сигнал інтерпретатору для обробки символу (або групи символів), що знаходяться після знаку
екранування особливим чином. Наприклад:
'n' в строці буде обролена як звичайна літера (вона може бути виведена на екран, конкатенована тощо)
'\n' n після символу екранування буде сприйнята інтерпретатором як символ переводу рядка (enter).
Загальне призначення екранування - можливість використання спеціальних символів та конструкцій мови у якості звичайних символів.

 2. Які засоби оголошення функцій ви знаєте?

 Function declaration - оголошення функції (синтаксис function functionName (args) {...})
Доступні для використання у коді без залежності (з оглядом на області видимості) від місця декларування (т.я. інтерпретатор запам'ятовує усі функції, оголошені
як function declaration до виконання коду)
Function expression - запис функції у змінну (синтаксис const name = function (args) {...})
Named function expression - запис іменованої функції у змінну (синтаксис const name = function functionName (args) {...})
На відміну від function expression доступні для рекурсивного виклику, т.я. мають ім'я.

 3. Що таке hoisting, як він працює для змінних та функцій?

 Hoisting - це вбудований механізм визначення порядку виконання того чи іншого коду без залежності від його фізичного розташування у файлі програми.
Для змінних: якщо змінна задекларована в будь-якому місці коду (у урахуванням областей видимості) - вона буде створена інтерпретатором ДО виконання основного коду

 Приклад:
console.log(variable)
let variable = 'value';
console.log(variable)
Буде виконано в наступній послідовності:
let variable;
console.log(variable); //output: undefined;
variable = 'value';
console.log(variable); //output: value;

 Таким чином, оголошення змінної буде "піднято" у приорітеті виконання коду.

 Для функцій:
 Функції, оголошені за допомогою function declaration також будуть "підняті" інтерпретатором при виконанні.

 Приклад:

 doSomething();
 function doSomething() {// ...code here...}

 Буде виконано без помилок, т.я. інтерпретатор зпочатку зчитає та запам'ятає всі оголошені функції, а потім виконуватиме інший код.

Задача:
Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому
Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем
(у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
*/


function createNewUser () {

    const newUser = {
        _firstName: '',
        _lastName: '',
        getLogin() {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },
        getPassword() {
            return (this._firstName[0].toLocaleUpperCase() + this._lastName.toLocaleLowerCase() + this.birthDate[2])
        },
        getAge() {
            let currDate = new Date();
            if ((currDate.getFullYear() - this.birthDate[2]) > 0) {
                if (currDate.getMonth() + 1 - this.birthDate[1] <= 0) {
                    return (currDate.getDate() - this.birthDate[0] >= 0) ? currDate.getFullYear() - this.birthDate[2] : currDate.getFullYear() - this.birthDate[2] - 1;
                } else {
                    return currDate.getFullYear() - this.birthDate[2];
                }
                /*return ((currDate.getMonth() + 1 - this.birthDate[1]) >= 0 && (currDate.getDate() - this.birthDate[0]) >= 0) ? currDate.getFullYear() - this.birthDate[2] : currDate.getFullYear() - this.birthDate[2] - 1;*/
            } else {
                return console.log('Your age is less than 1 year!')
            }
        },
        setFirstName(value) {
            Object.defineProperty(this, '_firstName', {
                writable: true,
            });
            this._firstName = value;
            Object.defineProperty(this, '_firstName', {
                writable: false,
            });
        },
        setLastName(value) {
            Object.defineProperty(this, '_lastName', {
                writable: true,
            });
            this._lastName = value;
            Object.defineProperty(this, '_lastName', {
                writable: false,
            });
        },
        setBirthDate(value) {
            this.birthDate = value.split('.');
        },
        getFirstName() {
            return this._firstName;
        },
        getLastName() {
            return this._lastName;
        },
    };

    Object.defineProperties(newUser, {
        '_firstName': { writable: false},
        '_lastName': { writable: false},
    });

    newUser.setFirstName(prompt('Your name?'));
    newUser.setLastName(prompt('Your last name?'));
    newUser.setBirthDate(prompt('Your birth date?', 'DD.MM.YYYY'));
    return newUser;
}

let user1 = createNewUser();

console.log(user1.getLogin());
console.log(user1.getPassword());
console.log(user1.getAge());
console.log(user1);